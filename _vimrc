" .vimrc

" Vundle plugin manager
"   to install:
"     mkdir -p ~/.vim/bundle
"     git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
" list plugin to use
Plugin 'editorconfig/editorconfig-vim'
Plugin 'airblade/vim-gitgutter'
Plugin 'tpope/vim-surround'
Plugin 'preservim/nerdtree'
Plugin 'vim-airline/vim-airline'
Plugin 'tpope/vim-fugitive'

call vundle#end()
filetype plugin indent on

set fenc=utf-8
set autoread
set hidden
set noswapfile
set showcmd

set number
set cursorline
"set cursorcolumn
set virtualedit=onemore
set smartindent
set visualbell
set showmatch
set list
set listchars=tab:>\ ,extends:<
set laststatus=2
set wildmode=list:longest

syntax enable
"nnoremap j gj
"nnoremap k gk

set ignorecase
set smartcase
set incsearch
set wrapscan
set hlsearch
nmap <Esc><Esc> :nohlsearch<CR><Esc>

"set statusline+=%{fugitive#statusline()}

